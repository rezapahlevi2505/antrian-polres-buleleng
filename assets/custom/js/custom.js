$(document).ready(function () {
    doPoll();
    lastPoll();
    next();
    skip();

    panggil();

});

function play() {
    var data = $('#value_antrian').val();
    data = JSON.parse(data);
    var nomor = parseInt(data.nomor);

    if (nomor > 0) {
        play_testing();
    } else {
        swal.fire({
            position: 'center',
            type: 'warning',
            title: 'Tidak Ada Nomor Antrian',
        });
    }
}

function play_testing() {
    var data = $('#value_antrian').val();
    var loket = $('#loket').val();
    data = JSON.parse(data);
    loket = JSON.parse(loket);
    console.log('loket')
    console.log(loket)
    var number = parseInt(data.nomor);
    var output = [],
        sNumber = number.toString();
    var length = sNumber.length;
    var url_audio = $('#url-audio').val();
    var kode = url_audio + '/kode/' + loket.kode_jasa + '.mp3';
    var loket = url_audio + '/loket/' + loket.nama_file + '.mp3';

    var opening = url_audio + '/opening.mp3';
    var ending = url_audio + '/ending.mp3';
    var aud = document.getElementById("suara-panggilan");

    for (var i = 0, len = sNumber.length; i < len; i += 1) {
        output.push(+sNumber.charAt(i));
    }

    if (length === 3) {
        $('audio #panggilan').attr('src', opening);
        $('audio').get(0).load();
        $('audio').get(0).play();

        aud.onended = function () {
            $('audio #panggilan').attr('src', kode);
            $('audio').get(0).load();
            $('audio').get(0).play();
            aud.onended = function () {
                var pertama = output[0];
                var url_pertama = url_audio + '/ratusan/' + pertama + '.mp3';
                $('audio #panggilan').attr('src', url_pertama);
                $('audio').get(0).load();
                $('audio').get(0).play();
                aud.onended = function () {
                    var kedua = output[1];
                    if (kedua < 1) {
                        var ketiga = output[2];
                        if (ketiga > 0) {
                            var url_ketiga = url_audio + '/satuan/' + ketiga + '.mp3';
                            $('audio #panggilan').attr('src', url_ketiga);
                            $('audio').get(0).load();
                            $('audio').get(0).play();
                            aud.onended = function () {
                                $('audio #panggilan').attr('src', ending);
                                $('audio').get(0).load();
                                $('audio').get(0).play();
                                aud.onended = function () {
                                    $('audio').get(0).pause();
                                };
                            };
                        } else {
                            $('audio #panggilan').attr('src', ending);
                            $('audio').get(0).load();
                            $('audio').get(0).play();
                            aud.onended = function () {
                                $('audio #panggilan').attr('src', loket);
                                $('audio').get(0).load();
                                $('audio').get(0).play();
                                aud.onended = function () {
                                    $('audio').get(0).pause();
                                };
                            };
                        }
                    } else if (kedua > 1) {
                        var url_kedua = url_audio + '/puluhan/' + kedua + '.mp3';
                        $('audio #panggilan').attr('src', url_kedua);
                        $('audio').get(0).load();
                        $('audio').get(0).play();
                        aud.onended = function () {
                            var ketiga = output[2];
                            var url_ketiga = url_audio + '/satuan/' + ketiga + '.mp3';
                            $('audio #panggilan').attr('src', url_ketiga);
                            $('audio').get(0).load();
                            $('audio').get(0).play();
                            aud.onended = function () {
                                $('audio #panggilan').attr('src', ending);
                                $('audio').get(0).load();
                                $('audio').get(0).play();
                                aud.onended = function () {
                                    $('audio #panggilan').attr('src', loket);
                                    $('audio').get(0).load();
                                    $('audio').get(0).play();
                                    aud.onended = function () {
                                        $('audio').get(0).pause();
                                    };
                                };
                            };
                        };
                    } else {
                        var belasan = output[2];
                        var url_belasan = url_audio + '/belasan/' + belasan + '.mp3';
                        if (belasan == 0){
                            url_belasan = url_audio + '/puluhan/' + output[0] + '.mp3';
                        }
                        $('audio #panggilan').attr('src', url_belasan);
                        $('audio').get(0).load();
                        $('audio').get(0).play();
                        aud.onended = function () {
                            $('audio #panggilan').attr('src', ending);
                            $('audio').get(0).load();
                            $('audio').get(0).play();
                            aud.onended = function () {
                                $('audio #panggilan').attr('src', loket);
                                $('audio').get(0).load();
                                $('audio').get(0).play();
                                aud.onended = function () {
                                    $('audio').get(0).pause();
                                };
                            };
                        };
                    }
                };
            };
        };
    } else if (length === 2) {
        $('audio #panggilan').attr('src', opening);
        $('audio').get(0).load();
        $('audio').get(0).play();

        aud.onended = function () {
            $('audio #panggilan').attr('src', kode);
            $('audio').get(0).load();
            $('audio').get(0).play();
            aud.onended = function () {
                var pertama = output[0];
                if (pertama > 1) {
                    var url_pertama = url_audio + '/puluhan/' + pertama + '.mp3';
                    $('audio #panggilan').attr('src', url_pertama);
                    $('audio').get(0).load();
                    $('audio').get(0).play();
                    aud.onended = function () {
                        var kedua = output[1];
                        var url_kedua = url_audio + '/satuan/' + kedua + '.mp3';
                        $('audio #panggilan').attr('src', url_kedua);
                        $('audio').get(0).load();
                        $('audio').get(0).play();
                        aud.onended = function () {
                            $('audio #panggilan').attr('src', ending);
                            $('audio').get(0).load();
                            $('audio').get(0).play();
                            aud.onended = function () {
                                $('audio #panggilan').attr('src', loket);
                                $('audio').get(0).load();
                                $('audio').get(0).play();
                                aud.onended = function () {
                                    $('audio').get(0).pause();
                                };
                            };
                        };
                    };
                } else {
                    var belasan = output[1];
                    var url_belasan = url_audio + '/belasan/' + belasan + '.mp3';
                    if (belasan == 0){
                        url_belasan = url_audio + '/puluhan/' + output[0] + '.mp3';
                    }
                    $('audio #panggilan').attr('src', url_belasan);
                    $('audio').get(0).load();
                    $('audio').get(0).play();
                    aud.onended = function () {
                        $('audio #panggilan').attr('src', ending);
                        $('audio').get(0).load();
                        $('audio').get(0).play();
                        aud.onended = function () {
                            $('audio #panggilan').attr('src', loket);
                            $('audio').get(0).load();
                            $('audio').get(0).play();
                            aud.onended = function () {
                                $('audio').get(0).pause();
                            };
                        };
                    };
                }
            };
        };
    } else {
        $('audio #panggilan').attr('src', opening);
        $('audio').get(0).load();
        $('audio').get(0).play();

        aud.onended = function () {
            $('audio #panggilan').attr('src', kode);
            $('audio').get(0).load();
            $('audio').get(0).play();
            aud.onended = function () {
                var pertama = output[0];
                var url_pertama = url_audio + '/satuan/' + pertama + '.mp3';
                $('audio #panggilan').attr('src', url_pertama);
                $('audio').get(0).load();
                $('audio').get(0).play();
                aud.onended = function () {
                    $('audio #panggilan').attr('src', ending);
                    $('audio').get(0).load();
                    $('audio').get(0).play();
                    aud.onended = function () {
                        $('audio #panggilan').attr('src', loket);
                        $('audio').get(0).load();
                        $('audio').get(0).play();
                        aud.onended = function () {
                            $('audio').get(0).pause();
                        };
                    };
                };
            };
        };
    }
}

function panggil() {
    $('#panggil').on('click', function () {
        var data = $('#value_antrian').val();
        console.log(data)
        data = JSON.parse(data);
        console.log(data)
        var nomor = parseInt(data.nomor);

        if (nomor !== 0) {
            play();
        } else {
            swal.fire({
                position: 'center',
                type: 'warning',
                title: 'Antrian saat ini kosong',
            });
        }
    });
}

function next() {
    $('#next').on('click', function () {
        var id = $('#id_antrian').val();
        var status = $('#status_antrian').val();
        var url = $('#next_url').val();
        var url_audio = $('#url-audio').val();
        var data = {
            'id': id,
            'status': status
        };

        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            encode: 'true',
            success: function (data) {
                $('#nomor_antrian').html(data.nomor.nomor_label);
                $('#status_antrian').val(data.nomor.status);
                $('#value_antrian').val(JSON.stringify(data.nomor));
                $('#id_antrian').val(data.nomor.id);
                // var audio = url_audio + '/audio' + data.nomor.nomor + '.mp3';
                // $('audio #panggilan').attr('src', audio);

                console.log('sukses');
                if (data.nomor.id !== id) {
                    play();
                } else {
                    swal.fire({
                        position: 'center',
                        type: 'warning',
                        title: 'Antrian saat ini kosong',
                    });
                }
            },
            error: function (data) {
                console.log('gagal');
            }
        });
    });
}

function skip() {
    $('#skip').on('click', function () {
        var id = $('#id_antrian').val();
        var status = $('#status_antrian').val();
        var url = $('#skip_url').val();
        var url_audio = $('#url-audio').val();
        var data = {
            'id': id,
            'status': status
        };

        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            encode: 'true',
            success: function (data) {
                $('#nomor_antrian').html(data.nomor.nomor_label);
                $('#status_antrian').val(data.nomor.status);
                $('#value_antrian').val(JSON.stringify(data.nomor));
                $('#id_antrian').val(data.nomor.id);
                // var audio = url_audio + '/audio' + data.nomor.nomor + '.mp3';
                // $('audio #panggilan').attr('src', audio);

                console.log('sukses');
                if (data.nomor.id !== id) {
                    play();
                } else {
                    swal.fire({
                        position: 'center',
                        type: 'warning',
                        title: 'Antrian saat ini kosong',
                    });
                }
            },
            error: function (data) {
                console.log('gagal');
            }
        });
    });
}

function doPoll() {
    var url = $('#url').val();
    var id = $('#id_antrian').val();
    var nomor = $('#nomor_antrian').html();
    var url_audio = $('#url-audio').val();

    var audio = url_audio + '/audio' + nomor + '.mp3';
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        encode: 'true',
        success: function (data) {
            $('#nomor_antrian').html(data.nomor.nomor_label);
            $('#status_antrian').val(data.nomor.status);
            $('#value_antrian').val(JSON.stringify(data.nomor));
            $('#id_antrian').val(data.nomor.id);
            $('audio #panggilan').attr('src', audio);
            console.log(data.nomor.nomor);
            // if (id !== data.nomor.id){
            //     play();
            // }
        },
        error: function (data) {
            console.log('gagal');
        }
    });
    setTimeout(doPoll, 5000);


}

function lastPoll() {
    var url = $('#url-last').val();
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        encode: 'true',
        success: function (data) {
            $('#nomor_antrian_terakhir').html(data.nomor.nomor_label);
            $('#id_antrian_terakhir').val(data.nomor.id);
            $('#value_antrian_terakhir').val(JSON.stringify(data.nomor));
            console.log(data.nomor.nomor);
        },
        error: function (data) {
            console.log('gagal');
        }
    })
    setTimeout(lastPoll, 5000);
}