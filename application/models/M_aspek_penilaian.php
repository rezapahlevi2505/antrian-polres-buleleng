<?php

class M_aspek_penilaian extends CI_Model {

    public function get_data() {
        $this->db->select('tb_aspek_penilaian.*, tb_jasa.nama_jasa');
        $this->db->from('tb_aspek_penilaian');
        $this->db->join('tb_jasa', 'tb_jasa.id = tb_aspek_penilaian.id_jasa', 'left');
        return $this->db->get();
    }
    public function input_data($data,$table){
        $this->db->insert($table,$data);
    }
    public function delete_data($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }
    function update_data($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
    }

}
