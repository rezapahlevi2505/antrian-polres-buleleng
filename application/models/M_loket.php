<?php

class M_loket extends CI_Model {

    public function get_data() {
        $this->db->select('tb_loket.*, tb_jasa.nama_jasa, tb_jasa.kode_jasa');
        $this->db->from('tb_loket');
        $this->db->join('tb_jasa', 'tb_jasa.id = tb_loket.id_jasa', 'left');
        return $this->db->get();
    }
    public function get_data_filter($where) {
        $this->db->select('tb_loket.*, tb_jasa.nama_jasa, tb_jasa.kode_jasa');
        $this->db->from('tb_loket');
        $this->db->join('tb_jasa', 'tb_jasa.id = tb_loket.id_jasa', 'left');
        $this->db->where($where);
        return $this->db->get();
    }
    public function input_data($data,$table){
        $this->db->insert($table,$data);
    }
    public function delete_data($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }
    function update_data($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
    }

}
