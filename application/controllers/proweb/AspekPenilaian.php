<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AspekPenilaian extends CI_Controller
{

    public function __construct()
    {
        parent:: __construct();
        $this->load->model('m_aspek_penilaian');
        $this->load->model('m_admin');
        $this->load->model('m_jasa');
        $this->load->library('main');
        $this->main->check_admin();
    }

    public function index()
    {
        $data = $this->main->data_main();
        $data['admin'] = $this->m_admin->get_data()->result();
        $data['aspek'] = $this->m_aspek_penilaian->get_data()->result();
        $data['jasa'] = $this->m_jasa->get_data()->result();
        $this->template->set('jasa', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'management Jasa');
        $this->template->load_admin('aspek_penilaian/index', $data);
    }

    public function createprocess()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id_jasa', 'Jasa', 'required');
        $this->form_validation->set_rules('nama_penilaian', 'Nama Penilaian', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'id_jasa' => form_error('id_jasa'),
                    'nama_penilaian' => form_error('nama_penilaian'),
                )
            ));
        } else {
            $id_jasa = $this->input->post('id_jasa');
            $nama_penilaian = $this->input->post('nama_penilaian');


            $data = array(
                'id_jasa' => $id_jasa,
                'nama_penilaian' => $nama_penilaian,
            );
            $this->m_jasa->input_data($data, 'tb_aspek_penilaian');
            echo json_encode(array(
                'status' => 'success',
                'message' => 'data berhasil diinput'
            ));
        }
    }

    public function delete($id)
    {
        $where = array('id' => $id);
        $this->m_jasa->delete_data($where, 'tb_aspek_penilaian');
    }

    public function update()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('id_jasa', 'Jasa', 'required');
        $this->form_validation->set_rules('nama_penilaian', 'Nama Penilaian', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'id_jasa' => form_error('id_jasa'),
                    'nama_penilaian' => form_error('nama_penilaian'),
                )
            ));
        } else {
            $id_jasa = $this->input->post('id_jasa');
            $nama_penilaian = $this->input->post('nama_penilaian');

            $id = $this->input->post('id');

            $data = array(
                'id_jasa' => $id_jasa,
                'nama_penilaian' => $nama_penilaian,
            );

            $where = array(
                'id' => $id
            );

            $this->m_jasa->update_data($where, $data, 'tb_aspek_penilaian');
            echo json_encode(array(
                'status' => 'success',
                'message' => 'data berhasil di edit'
            ));

        }
    }
}
