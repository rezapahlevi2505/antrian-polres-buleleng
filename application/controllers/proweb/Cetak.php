<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak extends CI_Controller
{

    function __construct(){
        parent::__construct();

        $this->load->model('m_admin');
        $this->load->model('m_loket');
        $this->load->library('main');
        $this->main->check_admin();

    }

    public function index(){
        $id_loket = $this->main->id_loket();
        $loket = $this->m_loket->get_data_filter(array('tb_loket.id' => $id_loket))->row();
        $js = array(
            1 => 'cetak.js'
        );

        $css = array(
            0 => 'custom.css'
        );

        $where = array(
            'status' => 'aktif',
            'tanggal' => date('Y-m-d'),
            'id_loket' => $id_loket
        );

        $aktif = array(
            'status' => 'sudah_dipanggil',
            'tanggal' => date('Y-m-d')
        );

        $kondisi = 'kosong';

        $nomor = $this->db->where($where)->order_by('nomor')->get('tb_antrian')->row();

        if (empty($nomor)) {
            $nomor = $this->db->where('tanggal', date('Y-m-d'))->where('id_loket', $id_loket)->where_in('status', array('sudah_dipanggil','lewat'))->order_by('nomor', 'desc')->get('tb_antrian')->row();

            if (empty($nomor)){
                $data = array(
                    'nomor' => 0,
                    'nomor_label' => 0,
                    'tanggal' => date('Y-m-d'),
                    'status' => 'sudah_dipanggil',
                    'id_loket' => $id_loket,
                    'id_jasa' => $loket->id_jasa,
                );

                $this->db->insert('tb_antrian', $data);

                $nomor = $this->db->where('tanggal', date('Y-m-d'))->where('id_loket', $id_loket)->where_in('status', array('sudah_dipanggil','lewat'))->order_by('nomor', 'desc')->get('tb_antrian')->row();

            }
        } else {
            $kondisi = 'ada';
        }

        $terakhir = $this->db->where('tanggal', date('Y-m-d'))->where('id_loket', $id_loket)->order_by('nomor', 'desc')->get('tb_antrian')->row();

        if (empty($terakhir)){
            $terakhir = array(
                'id' => null,
                'nomor' => 'Tidak Ada Antrian',
                'nomor_label' => 'Tidak Ada Antrian'
            );
        }

        $data = $this->main->data_main();
        $data['js'] = $js;
        $data['css'] = $css;
        $data['antrian'] = $nomor;
        $data['terakhir'] = $terakhir;
        $data['loket'] = $loket;
        $data['admin'] = $this->m_admin->get_data()->result();
        $this->template->set('cetak', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'Antrian');
        $this->template->load_admin('cetak/index', $data);
    }

    public function print($id){
//        $this->load->library('pdf');
        $id_loket = $this->main->id_loket();
        $loket = $this->m_loket->get_data_filter(array('tb_loket.id' => $id_loket))->row();
        $antrian = $this->db->where('id', $id)->get('tb_antrian')->row();

        $data = array(
            'antrian' => $antrian,
            'loket' => $loket
        );
//        $this->pdf->setPaper('A4', 'potrait');
//        $this->pdf->filename = "Nomor ".$antrian->nomor.".pdf";
//        $this->pdf->load_view('admins/cetak/print', $data);
        $this->load->view('admins/cetak/print_baru', $data);
    }

    public function reset(){
        $id_loket = $this->main->id_loket();
        $loket = $this->m_loket->get_data_filter(array('tb_loket.id' => $id_loket))->row();

        $this->db->where('tanggal', date('Y-m-d'))->where('id_jasa', $loket->id_jasa)->delete('tb_antrian');
    }

    function print_sub($id){
//        $antrian = $this->db->where('id', $id)->get('tb_antrian')->row();
//
//        $data = array(
//            'antrian' => $antrian
//        );
//        $this->load->view('admins/cetak/print_sub', $data);
//
//        $html = $this->output->get_output();
//        $this->load->library('pdf2');
//        $this->dompdf->loadHtml($html);
//        $this->dompdf->setPaper('A4', 'portrait');
//        $this->dompdf->render();
//        $this->dompdf->stream("welcome.pdf", array("Attachment"=>0));

    }

//    function print_sub_71($id){
//        $this->load->library('pdfgenerator');
//        $antrian = $this->db->where('id', $id)->get('tb_antrian')->row();
//
//        $data = array(
//            'antrian' => $antrian
//        );
//        $html = $this->load->view('admins/cetak/print_sub', $data);
//        $html = $this->output->get_output();
//        $filename = 'cetak-'.time();
//        $this->pdfgenerator->generate($html, $filename, true, 'A4', 'portrait');
//
//    }
}
