<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ulang extends CI_Controller
{

    function __construct(){
        parent::__construct();

        $this->load->model('m_admin');
        $this->load->model('m_loket');
        $this->load->library('main');
        $this->main->check_admin();
    }

    public function index(){
        $id_loket = $this->main->id_loket();
        $loket = $this->m_loket->get_data_filter(array('tb_loket.id' => $id_loket))->row();
        $js = array(
            0 => 'ulang.js'
        );

        $css = array(
            0 => 'custom.css'
        );

        $data = $this->main->data_main();
        $data['js'] = $js;
        $data['css'] = $css;
        $data['loket'] = $loket;
        $data['admin'] = $this->m_admin->get_data()->result();
        $this->template->set('ulang', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'Antrian');
        $this->template->load_admin('ulang/index', $data);
    }
}
