<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antrian extends CI_Controller
{

    function __construct(){
        parent::__construct();

        $this->load->model('m_admin');
        $this->load->model('m_loket');
        $this->load->library('main');
        $this->main->check_admin();
    }

    public function index(){

        $id_loket = $this->main->id_loket();
        $loket = $this->m_loket->get_data_filter(array('tb_loket.id' => $id_loket))->row();
        $js = array(
            0 => 'custom.js'
        );

        $css = array(
            0 => 'custom.css'
        );

        $where = array(
            'status' => 'aktif',
            'tanggal' => date('Y-m-d'),
            'id_loket' => $id_loket
        );

        $aktif = array(
            'status' => 'sudah_dipanggil',
            'tanggal' => date('Y-m-d')
        );

        $kondisi = 'kosong';

        $nomor = $this->db->where($where)->order_by('nomor')->get('tb_antrian')->row();

        if (empty($nomor)) {
            $nomor = $this->db->where('tanggal', date('Y-m-d'))->where('id_loket', $id_loket)->where_in('status', array('sudah_dipanggil','lewat'))->order_by('nomor', 'desc')->get('tb_antrian')->row();

            if (empty($nomor)){
                $data = array(
                    'nomor' => 0,
                    'nomor_label' => 0,
                    'tanggal' => date('Y-m-d'),
                    'status' => 'sudah_dipanggil',
                    'id_loket' => $id_loket,
                    'id_jasa' => $loket->id_jasa,
                );

                $this->db->insert('tb_antrian', $data);

                $nomor = $this->db->where('tanggal', date('Y-m-d'))->where('id_loket', $id_loket)->where_in('status', array('sudah_dipanggil','lewat'))->order_by('nomor', 'desc')->get('tb_antrian')->row();

            }
        } else {
            $kondisi = 'ada';
        }

        $terakhir = $this->db->where('tanggal', date('Y-m-d'))->where('id_loket', $id_loket)->order_by('nomor', 'desc')->get('tb_antrian')->row();

        if (empty($terakhir)){
            $terakhir = array(
                'id' => null,
                'nomor' => 'Tidak Ada Antrian',
                'nomor_label' => 'Tidak Ada Antrian'
            );
        }

        $data = $this->main->data_main();
        $data['js'] = $js;
        $data['css'] = $css;
        $data['antrian'] = $nomor;
        $data['terakhir'] = $terakhir;
        $data['loket'] = $loket;
        $data['admin'] = $this->m_admin->get_data()->result();
        $this->template->set('antrian', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'Antrian');
        $this->template->load_admin('antrian/index', $data);
//        print_r($data);
//        echo json_encode($data);
    }
}
