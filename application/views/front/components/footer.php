<footer class="footer white-bg pos-r o-hidden bg-contain"
        data-bg-img="<?php echo base_url() ?>assets/template_front/images/pattern/01.png">
    <div class="round-p-animation"></div>
    <div class="primary-footer">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-lg-4">
                    <div class="ht-theme-info bg-contain bg-pos-r h-100 dark-bg text-white"
                         data-bg-img="<?php echo base_url() ?>assets/template_front/images/bg/02.png">
                        <div class="footer-logo"><a href="index-2.html"> <img class="img-center"
                                                                              src="<?php echo base_url() ?>assets/template_front/images/redsystem-logo-white.png"
                                                                              alt="Red System Logo"
                                                                              title="Red System Logo" width="234"
                                                                              height="25"> </a></div>
                        <p class="mb-3" align="justify"> <?php echo $footer->meta_description ?> <i
                                    class="fas fa-long-arrow-alt-right"></i></span></a>
                        <div class="social-icons social-border circle social-hover mt-5"><h4 class="title">Follow
                                Us</h4>
                            <ul class="list-inline">
                                <li class="social-facebook"><a
                                            href="<?php echo $facebook_link ?>"
                                            title="Red System" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li class="social-instagram"><a href="<?php echo $instagram_link ?>"
                                                                title="Red System" target="_blank"><i
                                                class="fab fa-instagram"></i></a></li>
                                <li class="social-twitter"><a href="<?php echo $twitter_link ?>"
                                                              title="Red System" target="_blank"><i
                                                class="fab fa-twitter"></i></a></li>
                                <li class="social-linkedin"><a href="<?php echo $linked_in_link ?>"
                                                               title="Red System"><i class="fab fa-linkedin-in"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 py-8 md-px-5">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 footer-list"><h4 class="title">Link Lainnya</h4>
                            <div class="row">
                                <div class="col-sm-7 margin-bottom-15">
                                    <ul class="list-unstyled">
                                        <li><a href="faq.html">FAQ</a></li>
                                        <li><a href="syarat-ketentuan.html">Syarat & Ketentuan</a></li>
                                        <li><a href="kebijakan-privasi.html">Kebijakan Privasi</a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-5">
                                    <ul class="list-unstyled">
                                        <li><a href="tentang-kami.html">Tentang Kami</a></li>
                                        <li><a href="kontak.html">Kontak Kami</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 sm-mt-5"><h4 class="title">Contact us</h4>
                            <ul class="media-icon list-unstyled">
                                <li><p class="mb-0"><i class="ti-map"></i> <?php echo $address ?></p></li>
                                <li><a href="mailto:<?php echo $email ?>"><i class="ti-email"></i>
                                        <?php echo $email ?></a></li>
                                <li><a href="tel:<?php echo $phone ?>"><i class="ti-mobile"></i> <?php echo $phone ?>
                                    </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="secondary-footer">
        <div class="container">
            <div class="copyright">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <span>&copy; 2016 - <?php echo date("Y"); ?> | PT. Guna Teknologi Nusantara</span></div>
                    <div class="col-md-6 text-md-right sm-mt-2"><span>by <a href="<?php echo base_url('/') ?>"
                                                                            target="_blank">Red System</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>