<nav id="mainmenu">
    <ul class="navbar-nav ml-auto mr-auto">
        <?php foreach ($menu_front as $key => $val) { ?>
            <?php if (empty($val['sub_menu'])) { ?>
                <li class=" nav-item
                    role=" button" aria-haspopup="true" aria-expanded="false"
                data-toggle="hover">
                <a class=" <?php echo $page->type == $key ? 'nav-link active' : 'nav-link' ?>"" href="<?php echo $val['route'] ?>">
                <?php echo $val['label'] ?>
                </a>
                </li>
            <?php } else { ?>
                <li class="nav-item dropdown" data-toggle="hover" data-key="<?php echo $key ?>">
                    <a
                            class="<?php echo $page->type == $key ? 'nav-link dropdown-toggle active' : 'nav-link dropdown-toggle ' ?>"
                            href="#" role="button"
                            data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"><?php echo $val['label'] ?></a>
                    <div class="dropdown-menu">
                        <ul class="list-unstyled">
                            <?php foreach ($val['sub_menu'] as $key2 => $val2) { ?>
                                <li><a href="<?php echo $val2['route'] ?>"><?php echo $val2['label'] ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </li>
            <?php } ?>
        <?php } ?>
    </ul>
</nav>