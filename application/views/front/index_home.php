
<div class="m-grid m-grid--hor m-grid--root m-page">

    <!-- BEGIN: Header -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Disabled</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>

    <!-- END: Header -->

    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">


        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-content">
                <input type="hidden" id="url" value="<?= base_url('/now') ?>">

                <div class="row">
                    <div class="col-xl-2"></div>
                    <div class="col-xl-8"> <!--begin:: Widgets/Support Tickets -->
                        <div class="m-portlet m-portlet--full-height ">
                            <input type="hidden" value="<?= $antrian->id ?>" id="id_antrian">
                            <div class="m-portlet__body">
                                <div class="m-widget3" style="text-align: center">
                                    <div class="m-widget3__item">
                                        <p  style="font-size: 300px" id="nomor_antrian"><?= $antrian->nomor ?></p>
                                        <p style="font-size: 60px">Nomor Antrian Saat Ini</p>
                                    </div>
                                </div>
                            </div>
                        </div> <!--end:: Widgets/Support Tickets --> </div>
                    <div class="col-xl-2"></div>
                </div>
            </div>
        </div>

    </div>

    <!-- end:: Body -->

    <!-- begin::Footer -->
    <footer class="m-grid__item		m-footer ">
        <div class="m-container m-container--fluid m-container--full-height m-page__container">
            <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
								2013 - <?=date('Y')?> © <a href="https://redsystem.id/" class="m-link">Red System</a>
							</span>
                </div>
                <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
                    <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
                        <li class="m-nav__item">
                            <p>
                                <span class="m-nav__link-text">Polres Buleleng</span><br>
                                Jl. Pramuka No.1, Banjar Jawa, Kec. Buleleng, Kabupaten Buleleng, Bali 81118
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- end::Footer -->
</div>