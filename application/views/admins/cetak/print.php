<style type="text/css">
    .table-data {
        /*border-left: 0.01em solid #ccc;*/
        /*border-right: 0;*/
        /*border-top: 0.01em solid #ccc;*/
        /*border-bottom: 0;*/
        border-collapse: collapse;
        width: 100%;
    }

    .table-data td,
    .table-data th {
        /*border-left: 0;*/
        /*border-right: 0.01em solid #ccc;*/
        /*border-top: 0;*/
        /*border-bottom: 0.01em solid #ccc;*/
        padding: 2px 4px;
        text-align: center !important;
        font-size: 12px;
    }
</style>

<?php $str = base_url();
$str = preg_replace('#^https?://#', '', rtrim($str,'/')); ?>
<table width="100%">
    <tr>
        <td style="text-align: center; font-size: 55px; font-family: Tahoma, sans-serif; font-weight: bold">
            SELAMAT DATANG
        </td>
    </tr>
    <tr>

        <td style="text-align: center; font-size: 21px; font-family: Tahoma, sans-serif; font-weight: bold">
            Sistem Antrian SIM Online Satpas Polres Buleleng
        </td>
    </tr>
</table>
<hr>
<table width="100%" style="margin: 0; padding: 0">
    <tbody>
    <tr>
        <td style="text-align: left; font-size: 20px; font-family: Tahoma, sans-serif; font-weight: bold"><?= date('Y-m-d') ?></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: center; font-size: 50px; font-family: Tahoma, sans-serif; font-weight: bold">
            ANTRIAN
        </td>
    </tr>
    <tr style="padding-bottom: 1em;">
        <td colspan="3" style="text-align: center; font-size: 300px; font-family: Tahoma, sans-serif; font-weight: bold;">
            <?= $antrian->nomor_label ?>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: center;">
            <img src="<?php echo FCPATH.'assets/polres/https.png' ?>" width="200px" style="padding-top: 0">
        </td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: center; font-size: 30px; font-family: Tahoma, sans-serif; font-weight: normal;">
            <?= $str ?>
        </td>
    </tr>
    <tr>

        <td colspan="3" style="text-align: center; font-size: 30px; font-family: Tahoma, sans-serif; font-weight: normal;">
            Lokasi pelayanan : <?= $loket->nama_loket ?>
        </td>
    </tr>
    </tbody>
</table>

<hr>
<table width="100%">
    <tbody>
    <tr>
        <td style="text-align: center; font-size: 25px; font-family: Tahoma, sans-serif; font-weight: normal">
            Jalan Surapati No. 122 Singaraja, Buleleng - Bali
            <!--            <br>-->
            <!--            Call Center (+62362 25471)-->
        </td>
    </tr>
    </tbody>
</table>
