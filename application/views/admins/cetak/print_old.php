<!DOCTYPE html>
<html lang="id">
<!--<head>-->
<meta charset="utf-8">
<style type="text/css">
    td {
        margin: 0; padding: 0;
        width:100%;
        height:100%;
    }

    tr td:last-child {
        margin: 0; padding: 0;
        width:100%;
        height:100%;
    }
</style>
<?php $str = base_url();
$str = preg_replace('#^https?://#', '', rtrim($str,'/')); ?>

<!--</head>-->


<table width="100%" style="margin-top: 0">
    <tbody>
    <tr>
        <td style="text-align: center; font-size: 15px; font-family: Tahoma, sans-serif; font-weight: bold">
            SELAMAT DATANG
        </td>
    </tr>
    <tr>
        <td style="text-align: center; font-size: 10px; font-family: Tahoma, sans-serif; font-weight: bold">
            Sistem Antrian SIM Online Satpas Polres Buleleng
        </td>
    </tr>
    </tbody>
</table>
<hr>

<table width="100%" style="margin: 0; padding: 0">
    <tbody>
    <tr>
        <td style="text-align: left; font-size: 70px; font-family: Tahoma, sans-serif; font-weight: bold"><?= date('Y-m-d') ?></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: center; font-size: 150px; font-family: Tahoma, sans-serif; font-weight: bold">
            ANTRIAN
        </td>
    </tr>
    <tr style="padding-bottom: 1em;">
        <td colspan="3" style="text-align: center; font-size: 590px; font-family: Tahoma, sans-serif; font-weight: normal;">
            <?= $antrian->nomor ?>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: center;">
            <img src="<?php echo FCPATH.'assets/polres/https.jpg' ?>" width="550px" style="padding-top: 0"><br>
            <p style="font-family: Roboto; font-size: 80px"><?= $str ?></p>
        </td>
    </tr>
    </tbody>
</table>
<hr>
<table width="100%">
    <tbody>
    <tr>
        <td style="text-align: center; font-size: 25px; font-family: Tahoma, sans-serif; font-weight: normal">
            Jalan Surapati No. 122 Singaraja, Buleleng - Bali
            <!--            <br>-->
            <!--            Call Center (+62362 25471)-->
        </td>
    </tr>
    </tbody>
</table>


<table width="100%">
    <tbody>

    <tr>
        <td colspan="6">
            <img src="<?= 'assets/polres/https.jpg' ?>" width="90">
            <hr>
        </td>
    </tr>
    </tfoot>
</table>