<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Dompdf\Dompdf;
class Pdf2
{
    public function __construct()
    {
        require_once dirname(__FILE__) . '/dompdf 7/autoload.inc.php';
        $pdf = new Dompdf(["isRemoteEnabled" => true]);
        $CI = &get_instance();
        $CI->dompdf = $pdf;
    }
} ?>