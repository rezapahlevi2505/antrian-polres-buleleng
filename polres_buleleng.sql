-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 13, 2023 at 08:44 AM
-- Server version: 5.5.64-MariaDB
-- PHP Version: 7.3.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `polres_buleleng`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE IF NOT EXISTS `tb_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `akses_level` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id`, `username`, `password`, `email`, `name`, `akses_level`) VALUES
(45, 'widi', '5f4dcc3b5aa765d61d8327deb882cf99', 'iwayanwidiastika@gmail.com', 'widias', 1),
(46, 'polres', 'e10adc3949ba59abbe56e057f20f883e', 'polres@buleleng.com', 'polres buleleng', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_antrian`
--

CREATE TABLE IF NOT EXISTS `tb_antrian` (
  `id` int(11) NOT NULL,
  `nomor` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status` enum('belum_dipanggil','sudah_dipanggil','kadaluarsa','aktif','lewat') DEFAULT NULL,
  `nomor_label` varchar(255) DEFAULT NULL,
  `id_loket` int(11) DEFAULT NULL,
  `id_jasa` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15703 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_antrian`
--

INSERT INTO `tb_antrian` (`id`, `nomor`, `tanggal`, `status`, `nomor_label`, `id_loket`, `id_jasa`) VALUES
(15228, 0, '2021-10-12', 'sudah_dipanggil', '0', NULL, 1),
(15229, 0, '2021-10-12', 'sudah_dipanggil', '0', NULL, 2),
(15230, 0, '2021-10-12', 'sudah_dipanggil', '0', NULL, 3),
(15231, 0, '2021-10-12', 'sudah_dipanggil', '0', NULL, 4),
(15232, 0, '2021-10-12', 'sudah_dipanggil', '0', NULL, 5),
(15233, 1, '2021-10-12', 'belum_dipanggil', 'A1', NULL, 1),
(15234, 1, '2021-10-12', 'belum_dipanggil', 'B1', NULL, 2),
(15235, 1, '2021-10-12', 'belum_dipanggil', 'C1', NULL, 3),
(15236, 1, '2021-10-12', 'belum_dipanggil', 'D1', NULL, 2),
(15237, 1, '2021-10-12', 'belum_dipanggil', 'E1', NULL, 5),
(15238, 2, '2021-10-12', 'belum_dipanggil', 'A2', NULL, 1),
(15239, 2, '2021-10-12', 'belum_dipanggil', 'B2', NULL, 2),
(15240, 2, '2021-10-12', 'belum_dipanggil', 'C2', NULL, 3),
(15241, 2, '2021-10-12', 'belum_dipanggil', 'D2', NULL, 2),
(15242, 2, '2021-10-12', 'belum_dipanggil', 'E2', NULL, 5),
(15243, 3, '2021-10-12', 'belum_dipanggil', 'C3', 3, 3),
(15244, 1, '2021-10-12', 'belum_dipanggil', 'D1', 2, 4),
(15245, 2, '2021-10-12', 'belum_dipanggil', 'D2', 2, 4),
(15246, 3, '2021-10-12', 'belum_dipanggil', 'E3', 5, 5),
(15247, 4, '2021-10-12', 'belum_dipanggil', 'E4', 5, 5),
(15248, 3, '2021-10-12', 'belum_dipanggil', 'A3', 1, 1),
(15249, 4, '2021-10-12', 'belum_dipanggil', 'A4', NULL, 1),
(15250, 5, '2021-10-12', 'belum_dipanggil', 'B5', 4, 2),
(15251, 4, '2021-10-12', 'belum_dipanggil', 'C4', 3, 3),
(15252, 5, '2021-10-12', 'belum_dipanggil', 'A5', 1, 1),
(15253, 5, '2021-10-12', 'belum_dipanggil', 'C5', 3, 3),
(15254, 6, '2021-10-12', 'belum_dipanggil', 'B6', 4, 2),
(15255, 6, '2021-10-12', 'belum_dipanggil', 'A6', NULL, 1),
(15256, 7, '2021-10-12', 'belum_dipanggil', 'A7', NULL, 1),
(15257, 8, '2021-10-12', 'belum_dipanggil', 'A8', NULL, 1),
(15258, 9, '2021-10-12', 'belum_dipanggil', 'A9', 1, 1),
(15259, 10, '2021-10-12', 'belum_dipanggil', 'A10', NULL, 1),
(15260, 5, '2021-10-12', 'belum_dipanggil', 'E5', 5, 5),
(15261, 6, '2021-10-12', 'belum_dipanggil', 'C6', 3, 3),
(15262, 11, '2021-10-12', 'belum_dipanggil', 'A11', NULL, 1),
(15263, 7, '2021-10-12', 'belum_dipanggil', 'C7', 3, 3),
(15264, 8, '2021-10-12', 'belum_dipanggil', 'C8', 3, 3),
(15265, 12, '2021-10-12', 'belum_dipanggil', 'A12', NULL, 1),
(15266, 7, '2021-10-12', 'belum_dipanggil', 'B7', 4, 2),
(15267, 13, '2021-10-12', 'belum_dipanggil', 'A13', 1, 1),
(15268, 3, '2021-10-12', 'belum_dipanggil', 'D3', 2, 4),
(15269, 14, '2021-10-12', 'belum_dipanggil', 'A14', 1, 1),
(15270, 6, '2021-10-12', 'belum_dipanggil', 'E6', 5, 5),
(15271, 4, '2021-10-12', 'belum_dipanggil', 'D4', 2, 4),
(15272, 15, '2021-10-12', 'belum_dipanggil', 'A15', 1, 1),
(15273, 9, '2021-10-12', 'belum_dipanggil', 'C9', 3, 3),
(15274, 10, '2021-10-12', 'belum_dipanggil', 'C10', 3, 3),
(15275, 11, '2021-10-12', 'belum_dipanggil', 'C11', 3, 3),
(15276, 16, '2021-10-12', 'belum_dipanggil', 'A16', 1, 1),
(15277, 17, '2021-10-12', 'belum_dipanggil', 'A17', NULL, 1),
(15278, 18, '2021-10-12', 'belum_dipanggil', 'A18', 1, 1),
(15279, 8, '2021-10-12', 'belum_dipanggil', 'B8', 4, 2),
(15280, 7, '2021-10-12', 'belum_dipanggil', 'E7', 5, 5),
(15281, 19, '2021-10-12', 'belum_dipanggil', 'A19', NULL, 1),
(15282, 20, '2021-10-12', 'belum_dipanggil', 'A20', NULL, 1),
(15283, 8, '2021-10-12', 'belum_dipanggil', 'E8', 5, 5),
(15284, 9, '2021-10-12', 'belum_dipanggil', 'B9', 4, 2),
(15285, 5, '2021-10-12', 'belum_dipanggil', 'D5', 2, 4),
(15286, 10, '2021-10-12', 'belum_dipanggil', 'B10', 4, 2),
(15287, 12, '2021-10-12', 'belum_dipanggil', 'C12', 3, 3),
(15288, 6, '2021-10-12', 'belum_dipanggil', 'D6', 2, 4),
(15289, 21, '2021-10-12', 'belum_dipanggil', 'A21', 1, 1),
(15290, 11, '2021-10-12', 'belum_dipanggil', 'B11', 4, 2),
(15291, 9, '2021-10-12', 'belum_dipanggil', 'E9', 5, 5),
(15292, 10, '2021-10-12', 'belum_dipanggil', 'E10', 5, 5),
(15293, 22, '2021-10-12', 'belum_dipanggil', 'A22', 1, 1),
(15294, 23, '2021-10-12', 'belum_dipanggil', 'A23', 1, 1),
(15295, 12, '2021-10-12', 'belum_dipanggil', 'B12', 4, 2),
(15296, 24, '2021-10-12', 'belum_dipanggil', 'A24', 1, 1),
(15297, 25, '2021-10-12', 'belum_dipanggil', 'A25', NULL, 1),
(15298, 26, '2021-10-12', 'belum_dipanggil', 'A26', NULL, 1),
(15299, 27, '2021-10-12', 'belum_dipanggil', 'A27', NULL, 1),
(15300, 28, '2021-10-12', 'belum_dipanggil', 'A28', NULL, 1),
(15301, 29, '2021-10-12', 'belum_dipanggil', 'A29', NULL, 1),
(15302, 30, '2021-10-12', 'belum_dipanggil', 'A30', NULL, 1),
(15303, 31, '2021-10-12', 'belum_dipanggil', 'A31', NULL, 1),
(15304, 32, '2021-10-12', 'belum_dipanggil', 'A32', NULL, 1),
(15305, 33, '2021-10-12', 'belum_dipanggil', 'A33', NULL, 1),
(15306, 34, '2021-10-12', 'belum_dipanggil', 'A34', NULL, 1),
(15307, 35, '2021-10-12', 'belum_dipanggil', 'A35', NULL, 1),
(15308, 36, '2021-10-12', 'belum_dipanggil', 'A36', 1, 1),
(15309, 13, '2021-10-12', 'belum_dipanggil', 'B13', 4, 2),
(15310, 14, '2021-10-12', 'belum_dipanggil', 'B14', 4, 2),
(15311, 15, '2021-10-12', 'belum_dipanggil', 'B15', 4, 2),
(15312, 37, '2021-10-12', 'belum_dipanggil', 'A37', 1, 1),
(15313, 13, '2021-10-12', 'belum_dipanggil', 'C13', 3, 3),
(15314, 7, '2021-10-12', 'belum_dipanggil', 'D7', 2, 4),
(15315, 38, '2021-10-12', 'belum_dipanggil', 'A38', NULL, 1),
(15316, 39, '2021-10-12', 'belum_dipanggil', 'A39', 1, 1),
(15317, 40, '2021-10-12', 'belum_dipanggil', 'A40', NULL, 1),
(15318, 14, '2021-10-12', 'belum_dipanggil', 'C14', 3, 3),
(15319, 41, '2021-10-12', 'belum_dipanggil', 'A41', NULL, 1),
(15320, 42, '2021-10-12', 'belum_dipanggil', 'A42', NULL, 1),
(15321, 43, '2021-10-12', 'belum_dipanggil', 'A43', NULL, 1),
(15322, 44, '2021-10-12', 'belum_dipanggil', 'A44', NULL, 1),
(15323, 45, '2021-10-12', 'belum_dipanggil', 'A45', NULL, 1),
(15324, 46, '2021-10-12', 'belum_dipanggil', 'A46', 1, 1),
(15325, 15, '2021-10-12', 'belum_dipanggil', 'C15', 3, 3),
(15326, 47, '2021-10-12', 'belum_dipanggil', 'A47', 1, 1),
(15327, 48, '2021-10-12', 'belum_dipanggil', 'A48', NULL, 1),
(15328, 16, '2021-10-12', 'belum_dipanggil', 'B16', 4, 2),
(15329, 49, '2021-10-12', 'belum_dipanggil', 'A49', 1, 1),
(15330, 16, '2021-10-12', 'belum_dipanggil', 'C16', 3, 3),
(15331, 50, '2021-10-12', 'belum_dipanggil', 'A50', 1, 1),
(15332, 17, '2021-10-12', 'belum_dipanggil', 'C17', 3, 3),
(15333, 51, '2021-10-12', 'belum_dipanggil', 'A51', 1, 1),
(15334, 52, '2021-10-12', 'belum_dipanggil', 'A52', 1, 1),
(15335, 17, '2021-10-12', 'belum_dipanggil', 'B17', 4, 2),
(15336, 18, '2021-10-12', 'belum_dipanggil', 'B18', 4, 2),
(15337, 8, '2021-10-12', 'belum_dipanggil', 'D8', 2, 4),
(15338, 19, '2021-10-12', 'belum_dipanggil', 'B19', 4, 2),
(15339, 9, '2021-10-12', 'belum_dipanggil', 'D9', 2, 4),
(15340, 53, '2021-10-12', 'belum_dipanggil', 'A53', 1, 1),
(15341, 18, '2021-10-12', 'belum_dipanggil', 'C18', 3, 3),
(15342, 11, '2021-10-12', 'belum_dipanggil', 'E11', 5, 5),
(15343, 54, '2021-10-12', 'belum_dipanggil', 'A54', 1, 1),
(15344, 19, '2021-10-12', 'belum_dipanggil', 'C19', 3, 3),
(15345, 55, '2021-10-12', 'belum_dipanggil', 'A55', 1, 1),
(15346, 20, '2021-10-12', 'belum_dipanggil', 'B20', 4, 2),
(15347, 12, '2021-10-12', 'belum_dipanggil', 'E12', 5, 5),
(15348, 10, '2021-10-12', 'belum_dipanggil', 'D10', 2, 4),
(15349, 20, '2021-10-12', 'belum_dipanggil', 'C20', 3, 3),
(15350, 56, '2021-10-12', 'belum_dipanggil', 'A56', 1, 1),
(15351, 57, '2021-10-12', 'belum_dipanggil', 'A57', 1, 1),
(15352, 58, '2021-10-12', 'belum_dipanggil', 'A58', NULL, 1),
(15353, 59, '2021-10-12', 'belum_dipanggil', 'A59', NULL, 1),
(15354, 60, '2021-10-12', 'belum_dipanggil', 'A60', 1, 1),
(15355, 21, '2021-10-12', 'belum_dipanggil', 'C21', 3, 3),
(15356, 61, '2021-10-12', 'belum_dipanggil', 'A61', NULL, 1),
(15357, 11, '2021-10-12', 'belum_dipanggil', 'D11', 2, 4),
(15358, 21, '2021-10-12', 'belum_dipanggil', 'B21', 4, 2),
(15359, 62, '2021-10-12', 'belum_dipanggil', 'A62', NULL, 1),
(15360, 63, '2021-10-12', 'belum_dipanggil', 'A63', NULL, 1),
(15361, 64, '2021-10-12', 'belum_dipanggil', 'A64', NULL, 1),
(15362, 65, '2021-10-12', 'belum_dipanggil', 'A65', NULL, 1),
(15363, 22, '2021-10-12', 'belum_dipanggil', 'C22', 3, 3),
(15364, 66, '2021-10-12', 'belum_dipanggil', 'A66', 1, 1),
(15365, 13, '2021-10-12', 'belum_dipanggil', 'E13', 5, 5),
(15366, 22, '2021-10-12', 'belum_dipanggil', 'B22', 4, 2),
(15367, 12, '2021-10-12', 'belum_dipanggil', 'D12', 2, 4),
(15368, 67, '2021-10-12', 'belum_dipanggil', 'A67', 1, 1),
(15369, 68, '2021-10-12', 'belum_dipanggil', 'A68', 1, 1),
(15370, 69, '2021-10-12', 'belum_dipanggil', 'A69', NULL, 1),
(15371, 70, '2021-10-12', 'belum_dipanggil', 'A70', NULL, 1),
(15372, 71, '2021-10-12', 'belum_dipanggil', 'A71', NULL, 1),
(15373, 72, '2021-10-12', 'belum_dipanggil', 'A72', NULL, 1),
(15374, 73, '2021-10-12', 'belum_dipanggil', 'A73', 1, 1),
(15375, 23, '2021-10-12', 'belum_dipanggil', 'C23', 3, 3),
(15376, 74, '2021-10-12', 'belum_dipanggil', 'A74', 1, 1),
(15377, 75, '2021-10-12', 'belum_dipanggil', 'A75', NULL, 1),
(15378, 76, '2021-10-12', 'belum_dipanggil', 'A76', 1, 1),
(15379, 77, '2021-10-12', 'belum_dipanggil', 'A77', 1, 1),
(15380, 78, '2021-10-12', 'belum_dipanggil', 'A78', NULL, 1),
(15381, 79, '2021-10-12', 'belum_dipanggil', 'A79', NULL, 1),
(15382, 80, '2021-10-12', 'belum_dipanggil', 'A80', NULL, 1),
(15383, 81, '2021-10-12', 'belum_dipanggil', 'A81', NULL, 1),
(15384, 82, '2021-10-12', 'belum_dipanggil', 'A82', NULL, 1),
(15385, 83, '2021-10-12', 'belum_dipanggil', 'A83', NULL, 1),
(15386, 84, '2021-10-12', 'belum_dipanggil', 'A84', NULL, 1),
(15387, 15, '2021-10-12', 'belum_dipanggil', 'D15', NULL, 2),
(15388, 14, '2021-10-12', 'belum_dipanggil', 'E14', NULL, 5),
(15389, 21, '2021-10-12', 'belum_dipanggil', 'B21', NULL, 2),
(15390, 85, '2021-10-12', 'belum_dipanggil', 'A85', 1, 1),
(15391, 86, '2021-10-12', 'belum_dipanggil', 'A86', NULL, 1),
(15392, 87, '2021-10-12', 'belum_dipanggil', 'A87', 1, 1),
(15393, 88, '2021-10-12', 'belum_dipanggil', 'A88', 1, 1),
(15394, 13, '2021-10-12', 'belum_dipanggil', 'D13', 2, 4),
(15395, 89, '2021-10-12', 'belum_dipanggil', 'A89', 1, 1),
(15396, 25, '2021-10-12', 'belum_dipanggil', 'B25', 4, 2),
(15397, 90, '2021-10-12', 'belum_dipanggil', 'A90', 1, 1),
(15398, 24, '2021-10-12', 'belum_dipanggil', 'C24', 3, 3),
(15399, 25, '2021-10-12', 'belum_dipanggil', 'C25', 3, 3),
(15400, 91, '2021-10-12', 'belum_dipanggil', 'A91', 1, 1),
(15401, 26, '2021-10-12', 'belum_dipanggil', 'B26', 4, 2),
(15402, 15, '2021-10-12', 'belum_dipanggil', 'E15', 5, 5),
(15403, 27, '2021-10-12', 'belum_dipanggil', 'B27', 4, 2),
(15404, 28, '2021-10-12', 'belum_dipanggil', 'B28', 4, 2),
(15405, 92, '2021-10-12', 'belum_dipanggil', 'A92', 1, 1),
(15406, 26, '2021-10-12', 'belum_dipanggil', 'B26', NULL, 2),
(15407, 30, '2021-10-12', 'belum_dipanggil', 'B30', 4, 2),
(15408, 93, '2021-10-12', 'belum_dipanggil', 'A93', 1, 1),
(15409, 17, '2021-10-12', 'belum_dipanggil', 'D17', NULL, 2),
(15410, 18, '2021-10-12', 'belum_dipanggil', 'D18', NULL, 2),
(15411, 19, '2021-10-12', 'belum_dipanggil', 'D19', NULL, 2),
(15412, 20, '2021-10-12', 'belum_dipanggil', 'D20', NULL, 2),
(15413, 21, '2021-10-12', 'belum_dipanggil', 'D21', NULL, 2),
(15414, 94, '2021-10-12', 'belum_dipanggil', 'A94', 1, 1),
(15415, 26, '2021-10-12', 'belum_dipanggil', 'C26', 3, 3),
(15416, 14, '2021-10-12', 'belum_dipanggil', 'D14', 2, 4),
(15417, 95, '2021-10-12', 'belum_dipanggil', 'A95', 1, 1),
(15418, 96, '2021-10-12', 'belum_dipanggil', 'A96', NULL, 1),
(15419, 97, '2021-10-12', 'belum_dipanggil', 'A97', NULL, 1),
(15420, 98, '2021-10-12', 'belum_dipanggil', 'A98', NULL, 1),
(15421, 99, '2021-10-12', 'belum_dipanggil', 'A99', NULL, 1),
(15422, 100, '2021-10-12', 'belum_dipanggil', 'A100', NULL, 1),
(15423, 101, '2021-10-12', 'belum_dipanggil', 'A101', NULL, 1),
(15424, 102, '2021-10-12', 'belum_dipanggil', 'A102', NULL, 1),
(15425, 36, '2021-10-12', 'belum_dipanggil', 'B36', 4, 2),
(15426, 103, '2021-10-12', 'belum_dipanggil', 'A103', NULL, 1),
(15427, 27, '2021-10-12', 'belum_dipanggil', 'C27', 3, 3),
(15428, 104, '2021-10-12', 'belum_dipanggil', 'A104', NULL, 1),
(15429, 28, '2021-10-12', 'belum_dipanggil', 'C28', NULL, 3),
(15430, 105, '2021-10-12', 'belum_dipanggil', 'A105', 1, 1),
(15431, 106, '2021-10-12', 'belum_dipanggil', 'A106', NULL, 1),
(15432, 107, '2021-10-12', 'belum_dipanggil', 'A107', 1, 1),
(15433, 108, '2021-10-12', 'belum_dipanggil', 'A108', 1, 1),
(15434, 109, '2021-10-12', 'belum_dipanggil', 'A109', NULL, 1),
(15435, 110, '2021-10-12', 'belum_dipanggil', 'A110', NULL, 1),
(15436, 111, '2021-10-12', 'belum_dipanggil', 'A111', NULL, 1),
(15437, 15, '2021-10-12', 'belum_dipanggil', 'D15', 2, 4),
(15438, 112, '2021-10-12', 'belum_dipanggil', 'A112', 1, 1),
(15439, 29, '2021-10-12', 'belum_dipanggil', 'C29', 3, 3),
(15440, 113, '2021-10-12', 'belum_dipanggil', 'A113', NULL, 1),
(15441, 16, '2021-10-12', 'belum_dipanggil', 'E16', 5, 5),
(15442, 114, '2021-10-12', 'belum_dipanggil', 'A114', NULL, 1),
(15443, 115, '2021-10-12', 'belum_dipanggil', 'A115', NULL, 1),
(15444, 30, '2021-10-12', 'belum_dipanggil', 'C30', NULL, 3),
(15445, 116, '2021-10-12', 'belum_dipanggil', 'A116', 1, 1),
(15446, 37, '2021-10-12', 'belum_dipanggil', 'B37', 4, 2),
(15447, 117, '2021-10-12', 'belum_dipanggil', 'A117', 1, 1),
(15448, 30, '2021-10-12', 'belum_dipanggil', 'B30', NULL, 2),
(15449, 31, '2021-10-12', 'belum_dipanggil', 'B31', NULL, 2),
(15450, 32, '2021-10-12', 'belum_dipanggil', 'B32', NULL, 2),
(15451, 33, '2021-10-12', 'belum_dipanggil', 'B33', NULL, 2),
(15452, 17, '2021-10-12', 'belum_dipanggil', 'E17', NULL, 5),
(15453, 31, '2021-10-12', 'belum_dipanggil', 'C31', NULL, 3),
(15454, 32, '2021-10-12', 'belum_dipanggil', 'C32', NULL, 3),
(15455, 118, '2021-10-12', 'belum_dipanggil', 'A118', NULL, 1),
(15456, 119, '2021-10-12', 'belum_dipanggil', 'A119', NULL, 1),
(15457, 120, '2021-10-12', 'belum_dipanggil', 'A120', 1, 1),
(15458, 42, '2021-10-12', 'belum_dipanggil', 'B42', 4, 2),
(15459, 43, '2021-10-12', 'belum_dipanggil', 'B43', 4, 2),
(15460, 18, '2021-10-12', 'belum_dipanggil', 'E18', 5, 5),
(15461, 24, '2021-10-12', 'belum_dipanggil', 'D24', NULL, 2),
(15462, 36, '2021-10-12', 'belum_dipanggil', 'B36', NULL, 2),
(15463, 25, '2021-10-12', 'belum_dipanggil', 'D25', NULL, 2),
(15464, 33, '2021-10-12', 'belum_dipanggil', 'C33', NULL, 3),
(15492, 0, '2021-10-14', 'sudah_dipanggil', '0', 1, 1),
(15493, 0, '2021-10-14', 'sudah_dipanggil', '0', NULL, 2),
(15494, 0, '2021-10-14', 'sudah_dipanggil', '0', NULL, 3),
(15495, 0, '2021-10-14', 'sudah_dipanggil', '0', NULL, 4),
(15496, 0, '2021-10-14', 'sudah_dipanggil', '0', NULL, 5),
(15497, 1, '2021-10-14', 'belum_dipanggil', 'A1', 1, 1),
(15498, 0, '2021-10-18', 'sudah_dipanggil', '0', NULL, 1),
(15499, 0, '2021-10-18', 'sudah_dipanggil', '0', NULL, 2),
(15500, 0, '2021-10-18', 'sudah_dipanggil', '0', NULL, 3),
(15501, 0, '2021-10-18', 'sudah_dipanggil', '0', NULL, 4),
(15502, 0, '2021-10-18', 'sudah_dipanggil', '0', NULL, 5),
(15503, 0, '2021-10-18', 'sudah_dipanggil', '0', 1, 1),
(15554, 0, '2021-11-02', 'sudah_dipanggil', '0', NULL, 1),
(15555, 0, '2021-11-02', 'sudah_dipanggil', '0', NULL, 2),
(15556, 0, '2021-11-02', 'sudah_dipanggil', '0', NULL, 3),
(15557, 0, '2021-11-02', 'sudah_dipanggil', '0', NULL, 4),
(15558, 0, '2021-11-02', 'sudah_dipanggil', '0', NULL, 5),
(15559, 1, '2021-11-02', 'belum_dipanggil', 'A1', 1, 1),
(15560, 2, '2021-11-02', 'belum_dipanggil', 'A2', 1, 1),
(15561, 1, '2021-11-02', 'belum_dipanggil', 'B1', 4, 2),
(15562, 3, '2021-11-02', 'belum_dipanggil', 'A3', 1, 1),
(15563, 1, '2021-11-02', 'belum_dipanggil', 'D1', 2, 4),
(15564, 1, '2021-11-02', 'belum_dipanggil', 'C1', 3, 3),
(15565, 2, '2021-11-02', 'belum_dipanggil', 'D2', 2, 4),
(15566, 4, '2021-11-02', 'belum_dipanggil', 'A4', 1, 1),
(15567, 2, '2021-11-02', 'belum_dipanggil', 'B2', 4, 2),
(15568, 5, '2021-11-02', 'belum_dipanggil', 'A5', 1, 1),
(15569, 1, '2021-11-02', 'belum_dipanggil', 'E1', 5, 5),
(15570, 2, '2021-11-02', 'belum_dipanggil', 'C2', 3, 3),
(15571, 0, '2021-11-09', 'sudah_dipanggil', '0', NULL, 1),
(15572, 0, '2021-11-09', 'sudah_dipanggil', '0', NULL, 2),
(15573, 0, '2021-11-09', 'sudah_dipanggil', '0', NULL, 3),
(15574, 0, '2021-11-09', 'sudah_dipanggil', '0', NULL, 4),
(15575, 0, '2021-11-09', 'sudah_dipanggil', '0', NULL, 5),
(15576, 0, '2021-11-13', 'sudah_dipanggil', '0', NULL, 1),
(15577, 0, '2021-11-13', 'sudah_dipanggil', '0', NULL, 2),
(15578, 0, '2021-11-13', 'sudah_dipanggil', '0', NULL, 3),
(15579, 0, '2021-11-13', 'sudah_dipanggil', '0', NULL, 4),
(15580, 0, '2021-11-13', 'sudah_dipanggil', '0', NULL, 5),
(15581, 0, '2021-12-17', 'sudah_dipanggil', '0', 1, 1),
(15582, 1, '2021-12-17', 'sudah_dipanggil', 'A1', 1, 1),
(15583, 2, '2021-12-17', 'lewat', 'A2', 1, 1),
(15584, 3, '2021-12-17', 'aktif', 'A3', 1, 1),
(15585, 0, '2021-12-17', 'sudah_dipanggil', '0', NULL, 2),
(15586, 0, '2021-12-17', 'sudah_dipanggil', '0', NULL, 3),
(15587, 0, '2021-12-17', 'sudah_dipanggil', '0', NULL, 4),
(15588, 0, '2021-12-17', 'sudah_dipanggil', '0', NULL, 5),
(15589, 0, '2021-12-27', 'sudah_dipanggil', '0', NULL, 1),
(15590, 0, '2021-12-27', 'sudah_dipanggil', '0', NULL, 2),
(15591, 0, '2021-12-27', 'sudah_dipanggil', '0', NULL, 3),
(15592, 0, '2021-12-27', 'sudah_dipanggil', '0', NULL, 4),
(15593, 0, '2021-12-27', 'sudah_dipanggil', '0', NULL, 5),
(15594, 0, '2021-12-28', 'sudah_dipanggil', '0', NULL, 1),
(15595, 0, '2021-12-28', 'sudah_dipanggil', '0', NULL, 2),
(15596, 0, '2021-12-28', 'sudah_dipanggil', '0', NULL, 3),
(15597, 0, '2021-12-28', 'sudah_dipanggil', '0', NULL, 4),
(15598, 0, '2021-12-28', 'sudah_dipanggil', '0', NULL, 5),
(15599, 0, '2022-02-04', 'sudah_dipanggil', '0', NULL, 1),
(15600, 0, '2022-02-04', 'sudah_dipanggil', '0', NULL, 2),
(15601, 0, '2022-02-04', 'sudah_dipanggil', '0', NULL, 3),
(15602, 0, '2022-02-04', 'sudah_dipanggil', '0', NULL, 4),
(15603, 0, '2022-02-04', 'sudah_dipanggil', '0', NULL, 5),
(15604, 0, '2022-02-04', 'sudah_dipanggil', '0', 1, 1),
(15605, 0, '2022-02-05', 'sudah_dipanggil', '0', NULL, 1),
(15606, 0, '2022-02-05', 'sudah_dipanggil', '0', NULL, 2),
(15607, 0, '2022-02-05', 'sudah_dipanggil', '0', NULL, 3),
(15608, 0, '2022-02-05', 'sudah_dipanggil', '0', NULL, 4),
(15609, 0, '2022-02-05', 'sudah_dipanggil', '0', NULL, 5),
(15610, 0, '2022-02-06', 'sudah_dipanggil', '0', NULL, 1),
(15611, 0, '2022-02-06', 'sudah_dipanggil', '0', NULL, 2),
(15612, 0, '2022-02-06', 'sudah_dipanggil', '0', NULL, 3),
(15613, 0, '2022-02-06', 'sudah_dipanggil', '0', NULL, 4),
(15614, 0, '2022-02-06', 'sudah_dipanggil', '0', NULL, 5),
(15615, 0, '2022-02-07', 'sudah_dipanggil', '0', NULL, 1),
(15616, 0, '2022-02-07', 'sudah_dipanggil', '0', NULL, 1),
(15617, 0, '2022-02-07', 'sudah_dipanggil', '0', NULL, 2),
(15618, 0, '2022-02-07', 'sudah_dipanggil', '0', NULL, 2),
(15619, 0, '2022-02-07', 'sudah_dipanggil', '0', NULL, 3),
(15620, 0, '2022-02-07', 'sudah_dipanggil', '0', NULL, 3),
(15621, 0, '2022-02-07', 'sudah_dipanggil', '0', NULL, 4),
(15622, 0, '2022-02-07', 'sudah_dipanggil', '0', NULL, 4),
(15623, 0, '2022-02-07', 'sudah_dipanggil', '0', NULL, 5),
(15624, 0, '2022-02-07', 'sudah_dipanggil', '0', NULL, 5),
(15625, 0, '2022-02-08', 'sudah_dipanggil', '0', NULL, 1),
(15626, 0, '2022-02-08', 'sudah_dipanggil', '0', NULL, 2),
(15627, 0, '2022-02-08', 'sudah_dipanggil', '0', NULL, 3),
(15628, 0, '2022-02-08', 'sudah_dipanggil', '0', NULL, 4),
(15629, 0, '2022-02-08', 'sudah_dipanggil', '0', NULL, 5),
(15630, 0, '2022-02-26', 'sudah_dipanggil', '0', NULL, 1),
(15631, 0, '2022-02-26', 'sudah_dipanggil', '0', NULL, 2),
(15632, 0, '2022-02-26', 'sudah_dipanggil', '0', NULL, 3),
(15633, 0, '2022-02-26', 'sudah_dipanggil', '0', NULL, 4),
(15634, 0, '2022-02-26', 'sudah_dipanggil', '0', NULL, 5),
(15635, 0, '2022-03-29', 'sudah_dipanggil', '0', NULL, 1),
(15636, 0, '2022-03-29', 'sudah_dipanggil', '0', NULL, 2),
(15637, 0, '2022-03-29', 'sudah_dipanggil', '0', NULL, 3),
(15638, 0, '2022-03-29', 'sudah_dipanggil', '0', NULL, 4),
(15639, 0, '2022-03-29', 'sudah_dipanggil', '0', NULL, 5),
(15640, 0, '2022-04-19', 'sudah_dipanggil', '0', NULL, 1),
(15641, 0, '2022-04-19', 'sudah_dipanggil', '0', NULL, 2),
(15642, 0, '2022-04-19', 'sudah_dipanggil', '0', NULL, 3),
(15643, 0, '2022-04-19', 'sudah_dipanggil', '0', NULL, 4),
(15644, 0, '2022-04-19', 'sudah_dipanggil', '0', NULL, 5),
(15645, 0, '2022-08-29', 'sudah_dipanggil', '0', NULL, 1),
(15646, 0, '2022-08-29', 'sudah_dipanggil', '0', NULL, 1),
(15647, 0, '2022-08-29', 'sudah_dipanggil', '0', NULL, 1),
(15648, 0, '2022-08-29', 'sudah_dipanggil', '0', NULL, 2),
(15649, 0, '2022-08-29', 'sudah_dipanggil', '0', NULL, 2),
(15650, 0, '2022-08-29', 'sudah_dipanggil', '0', NULL, 2),
(15651, 0, '2022-08-29', 'sudah_dipanggil', '0', NULL, 3),
(15652, 0, '2022-08-29', 'sudah_dipanggil', '0', NULL, 3),
(15653, 0, '2022-08-29', 'sudah_dipanggil', '0', NULL, 3),
(15654, 0, '2022-08-29', 'sudah_dipanggil', '0', NULL, 4),
(15655, 0, '2022-08-29', 'sudah_dipanggil', '0', NULL, 4),
(15656, 0, '2022-08-29', 'sudah_dipanggil', '0', NULL, 4),
(15657, 0, '2022-08-29', 'sudah_dipanggil', '0', NULL, 5),
(15658, 0, '2022-08-29', 'sudah_dipanggil', '0', NULL, 5),
(15659, 0, '2022-08-29', 'sudah_dipanggil', '0', NULL, 5),
(15660, 0, '2022-11-09', 'sudah_dipanggil', '0', NULL, 1),
(15661, 0, '2022-11-09', 'sudah_dipanggil', '0', NULL, 2),
(15662, 0, '2022-11-09', 'sudah_dipanggil', '0', NULL, 3),
(15663, 0, '2022-11-09', 'sudah_dipanggil', '0', NULL, 4),
(15664, 0, '2022-11-09', 'sudah_dipanggil', '0', NULL, 5),
(15665, 0, '2022-11-09', 'sudah_dipanggil', '0', 1, 1),
(15666, 0, '2022-11-10', 'sudah_dipanggil', '0', NULL, 1),
(15667, 0, '2022-11-10', 'sudah_dipanggil', '0', NULL, 2),
(15668, 0, '2022-11-10', 'sudah_dipanggil', '0', NULL, 3),
(15669, 0, '2022-11-10', 'sudah_dipanggil', '0', NULL, 4),
(15670, 0, '2022-11-10', 'sudah_dipanggil', '0', NULL, 5),
(15671, 0, '2022-12-07', 'sudah_dipanggil', '0', NULL, 1),
(15672, 0, '2022-12-07', 'sudah_dipanggil', '0', NULL, 2),
(15673, 0, '2022-12-07', 'sudah_dipanggil', '0', NULL, 3),
(15674, 0, '2022-12-07', 'sudah_dipanggil', '0', NULL, 4),
(15675, 0, '2022-12-07', 'sudah_dipanggil', '0', NULL, 5),
(15676, 0, '2023-03-03', 'sudah_dipanggil', '0', NULL, 1),
(15677, 0, '2023-03-03', 'sudah_dipanggil', '0', NULL, 1),
(15678, 0, '2023-03-03', 'sudah_dipanggil', '0', NULL, 2),
(15679, 0, '2023-03-03', 'sudah_dipanggil', '0', NULL, 2),
(15680, 0, '2023-03-03', 'sudah_dipanggil', '0', NULL, 3),
(15681, 0, '2023-03-03', 'sudah_dipanggil', '0', NULL, 3),
(15682, 0, '2023-03-03', 'sudah_dipanggil', '0', NULL, 4),
(15683, 0, '2023-03-03', 'sudah_dipanggil', '0', NULL, 4),
(15684, 0, '2023-03-03', 'sudah_dipanggil', '0', NULL, 5),
(15685, 0, '2023-03-03', 'sudah_dipanggil', '0', NULL, 5),
(15686, 2, '2023-03-03', 'belum_dipanggil', 'B2', 4, 2),
(15687, 2, '2023-03-03', 'belum_dipanggil', 'A2', 1, 1),
(15688, 2, '2023-03-03', 'belum_dipanggil', 'E2', 5, 5),
(15689, 0, '2023-07-21', 'sudah_dipanggil', '0', NULL, 1),
(15690, 0, '2023-07-21', 'sudah_dipanggil', '0', NULL, 2),
(15691, 0, '2023-07-21', 'sudah_dipanggil', '0', NULL, 3),
(15692, 0, '2023-07-21', 'sudah_dipanggil', '0', NULL, 4),
(15693, 0, '2023-07-21', 'sudah_dipanggil', '0', NULL, 5),
(15694, 0, '2023-09-13', 'sudah_dipanggil', '0', NULL, 1),
(15695, 0, '2023-09-13', 'sudah_dipanggil', '0', NULL, 2),
(15696, 0, '2023-09-13', 'sudah_dipanggil', '0', NULL, 3),
(15697, 0, '2023-09-13', 'sudah_dipanggil', '0', NULL, 4),
(15698, 0, '2023-09-13', 'sudah_dipanggil', '0', NULL, 5),
(15699, 0, '2023-09-13', 'sudah_dipanggil', '0', 4, 2),
(15700, 2, '2023-09-13', 'sudah_dipanggil', 'B2', 4, 2),
(15701, 3, '2023-09-13', 'lewat', 'B3', 4, 2),
(15702, 4, '2023-09-13', 'aktif', 'B4', 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_aspek_penilaian`
--

CREATE TABLE IF NOT EXISTS `tb_aspek_penilaian` (
  `id` int(11) NOT NULL,
  `id_jasa` int(11) DEFAULT NULL,
  `nama_penilaian` varchar(255) DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_aspek_penilaian`
--

INSERT INTO `tb_aspek_penilaian` (`id`, `id_jasa`, `nama_penilaian`, `nilai`) VALUES
(1, 1, 'Keramahan', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_jasa`
--

CREATE TABLE IF NOT EXISTS `tb_jasa` (
  `id` int(11) NOT NULL,
  `nama_jasa` varchar(255) DEFAULT NULL,
  `kode_jasa` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jasa`
--

INSERT INTO `tb_jasa` (`id`, `nama_jasa`, `kode_jasa`, `keterangan`, `color`) VALUES
(1, 'Foto SIM', 'A', 'Jasa Foto SIM', '#49fd8e'),
(2, 'BPKB', 'B', 'Jasa Pembuatan BPKB', '#1fa4d1'),
(3, 'Sidik Jari', 'C', 'Jasa Pembuatan Kartu Sidik Jari', '#fbbf18'),
(4, 'SKCK', 'D', 'Jasa Pembuatan SKCK', '#f2ff38'),
(5, 'Surat Kehilangan', 'E', 'Jasa Pengurusan Surat Kehilangan', '#f769a9');

-- --------------------------------------------------------

--
-- Table structure for table `tb_loket`
--

CREATE TABLE IF NOT EXISTS `tb_loket` (
  `id` int(11) NOT NULL,
  `nama_loket` varchar(255) DEFAULT NULL,
  `id_jasa` int(11) DEFAULT NULL,
  `nama_file` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_loket`
--

INSERT INTO `tb_loket` (`id`, `nama_loket`, `id_jasa`, `nama_file`) VALUES
(1, 'Ruang Foto SIM', 1, 'sim'),
(2, 'Loket SKCK', 4, 'skck'),
(3, 'Loket Sidik Jari', 3, 'sidik-jari'),
(4, 'Loket BPKB', 2, 'bpkb'),
(5, 'Loket Surat Kehilangan', 5, 'surat-kehilangan');

-- --------------------------------------------------------

--
-- Table structure for table `tb_nilai_aspek`
--

CREATE TABLE IF NOT EXISTS `tb_nilai_aspek` (
  `id` int(11) NOT NULL,
  `id_aspek` int(11) DEFAULT NULL,
  `jawaban` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_penilaian`
--

CREATE TABLE IF NOT EXISTS `tb_penilaian` (
  `id` int(11) NOT NULL,
  `id_antrian` int(11) DEFAULT NULL,
  `id_penilaian` int(11) DEFAULT NULL,
  `jawaban` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_antrian`
--
ALTER TABLE `tb_antrian`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_aspek_penilaian`
--
ALTER TABLE `tb_aspek_penilaian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_jasa`
--
ALTER TABLE `tb_jasa`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `kode_jasa` (`kode_jasa`) USING BTREE;

--
-- Indexes for table `tb_loket`
--
ALTER TABLE `tb_loket`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tb_nilai_aspek`
--
ALTER TABLE `tb_nilai_aspek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_penilaian`
--
ALTER TABLE `tb_penilaian`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `tb_antrian`
--
ALTER TABLE `tb_antrian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15703;
--
-- AUTO_INCREMENT for table `tb_aspek_penilaian`
--
ALTER TABLE `tb_aspek_penilaian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_jasa`
--
ALTER TABLE `tb_jasa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_loket`
--
ALTER TABLE `tb_loket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_nilai_aspek`
--
ALTER TABLE `tb_nilai_aspek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_penilaian`
--
ALTER TABLE `tb_penilaian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
