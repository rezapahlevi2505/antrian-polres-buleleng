$(document).ready(function () {
    doPoll();
    next();
    skip();

    panggil();

});

function play() {
    var nomor = $('#nomor_antrian').html();

    if (nomor < 126){
        $('audio').get(0).load();
        $('audio').get(0).play();
    }else {
        swal.fire({
            position: 'center',
            type: 'warning',
            title: 'Audio Tidak Ditemukan',
        });
    }
}

function panggil() {
    $('#panggil').on('click', function () {
        var nomor = $('#nomor_antrian').html();
        nomor = parseInt(nomor);

        if (nomor !== 0) {
            play();
        } else {
            swal.fire({
                position: 'center',
                type: 'warning',
                title: 'Antrian saat ini kosong',
            });
        }
    });
}

function next() {
    $('#next').on('click', function () {
        var id = $('#id_antrian').val();
        var status = $('#status_antrian').val();
        var url = $('#next_url').val();
        var url_audio = $('#url-audio').val();
        var data = {
            'id': id,
            'status': status
        };

        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            encode: 'true',
            success: function (data) {
                $('#nomor_antrian').html(data.nomor.nomor);
                $('#status_antrian').val(data.nomor.status);
                $('#id_antrian').val(data.nomor.id);
                var audio = url_audio + '/audio' + data.nomor.nomor + '.mp3';
                $('audio #panggilan').attr('src', audio);

                console.log('sukses');
                if (data.nomor.id !== id) {
                    play();
                } else {
                    swal.fire({
                        position: 'center',
                        type: 'warning',
                        title: 'Antrian saat ini kosong',
                    });
                }
            },
            error: function (data) {
                console.log('gagal');
            }
        });
    });
}

function skip() {
    $('#skip').on('click', function () {
        var id = $('#id_antrian').val();
        var status = $('#status_antrian').val();
        var url = $('#skip_url').val();
        var url_audio = $('#url-audio').val();
        var data = {
            'id': id,
            'status': status
        };

        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            encode: 'true',
            success: function (data) {
                $('#nomor_antrian').html(data.nomor.nomor);
                $('#status_antrian').val(data.nomor.status);
                $('#id_antrian').val(data.nomor.id);
                var audio = url_audio + '/audio' + data.nomor.nomor + '.mp3';
                $('audio #panggilan').attr('src', audio);

                console.log('sukses');
                if (data.nomor.id !== id) {
                    play();
                } else {
                    swal.fire({
                        position: 'center',
                        type: 'warning',
                        title: 'Antrian saat ini kosong',
                    });
                }
            },
            error: function (data) {
                console.log('gagal');
            }
        });
    });
}

function doPoll() {
    var url = $('#url').val();
    var id = $('#id_antrian').val();
    var nomor = $('#nomor_antrian').html();
    var url_audio = $('#url-audio').val();

    var audio = url_audio + '/audio' + nomor + '.mp3';
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        encode: 'true',
        success: function (data) {
            $('#nomor_antrian').html(data.nomor.nomor);
            $('#status_antrian').val(data.nomor.status);
            $('#id_antrian').val(data.nomor.id);
            $('audio #panggilan').attr('src', audio);
            console.log(data.nomor.nomor);
            // if (id !== data.nomor.id){
            //     play();
            // }
        },
        error: function (data) {
            console.log('gagal');
        }
    });
    setTimeout(doPoll, 5000);
}